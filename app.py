# app.py 

from routes import app
from socket_events import socketio

if __name__ == "__main__":
    socketio.run(app)
    # app.run()

# Run the code with `python app.py` in the virtual environment. 
