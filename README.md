## Whatsapp

This is a teaching project that intends to implement the basic functionality of Whatsapp. 

### Requirements
This implementation will eventually support the following:
- Users should be able to login with username and password
- After succesful login, they should see a list of chats and groups they are part of
- Clicking on the chat takes them to the chat screen where they can send and receive messages
- User's online or offline status is shown in their chat window
