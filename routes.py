from flask import Flask, render_template, request
app = Flask(__name__) # __main__ or app.py

app.config['DEBUG'] = True

@app.route('/') # decorator
def home():
    # return 'Hello World'
    return render_template('index.html')


@app.route('/chats') # decorator
def chats():
    # return 'Hello World'
    # print(request.args)
    username = request.args['username']
    print(username)
    return render_template('chats.html', username=username)
